module.exports = function (app) {

	// Index de la página
	app.get ('/', function (req, res, next) {
		// Render views/index.html
		res.render ('login');
	});

	//Login de la página
	app.get ('/login', function (req, res, next) {
		// Render views/index.html
		res.render ('login');
		next ();
	});

	// RUta para cerrar sesión
	app.get ('/salir', function (req, res, next) {
		// Render views/index.html
		res.render ('login');
		next ();
	});

	//Ruta para crear grupo
	app.get ('/grupo', function (req, res, next) {
		// Render views/index.html
		res.render ('grupo');
		next ();
	});

	//Ruta para asignar usuarios a grupo
	app.get ('/asignar', function (req, res, next) {
		// Render views/index.html
		res.render ('asignar');
		next ();
	});

	//Panel de actividades principal (msgBoard, calendar, contacts, groups)
	app.get ('/board', function (req, res, next) {
		// Render views/index.html
		res.render ('board');
		next ();
	});

	//Registro de la pagina
	app.get ('/register', function (req, res, next) {
		// Render views/index.html
		res.render ('register');
		next ();
	});
};

