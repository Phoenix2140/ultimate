(function () {

	var app = angular.module ( "app", [] );

	app.factory ( 'socket', function () {
		var socket = io ();
		return socket;
	} );

	app.controller ( 'indexController', function ( $scope, socket) {

	} );

	app.controller ('loginController', ['$scope', '$location', 'socket', function ($scope, $location, socket) {
		$scope.user 		= "";
		$scope.pass 		= "";

		$scope.login = function () {
			var datos = {
				user: $scope.user,
				pass: $scope.pass
			}

			socket.emit ('loginUser', datos);
		}

		socket.on ('loginUserResult', function (response) {
			if (response.user == $scope.user && response.pass == $scope.pass) {
				console.log ("usuario encontrado");
				console.log (response.user);

				if (typeof (Storage) !== 'undefined') {
					if (!sessionStorage.user || sessionStorage.user == "") {
						sessionStorage.user = response._id;
					}
					if (!sessionStorage.username || sessionStorage.username == "") {
						sessionStorage.username = response.user;
					}
				} else {
					console.log ("No hay soporte para session.storage");
				}

				window.location.replace("/board");

			} else {
				console.log ("usuario no encontrado, verificar contraseña");
			}
		} );

		

	} ] );

	app.controller ('registerController', ['$scope', 'socket', function ($scope, socket) {
		$scope.user 		= "";
		$scope.email  		= "";
		$scope.pass1		= "";
		$scope.pass2		= "";
		$scope.checkPass 	= false;
		$scope.checkUser	= false;

		socket.on ('searchUserResult', function (response) {
			if (response != null) {
				if (response.user == $scope.user) {
					$scope.checkUser	= true;
					console.log ("el usuario que intenta ingresar ya existe");
				} else {
					console.log ("el usuario que intenta ingresar no existe, por lo tanto se creará");
					$scope.checkUser	= false;
				}
			}
		} );

		$scope.registrar = function () {
			if($scope.pass1 == $scope.pass2){
				var pass = $scope.pass1;
				$scope.checkPass = false;
				console.log("Las contraseñas son iguales, todo OK.");
			} else{
				var pass = "";
				$scope.checkPass = true;
				console.log("Las contraseñas son distintas, ERROR");
			}

			var registro = {
				user: $scope.user,
				pass: pass,
				email: $scope.email,
				pos_x: "",
				pos_y: ""
			}

			//Comprobar que el usuario no exista
			socket.emit ('searchUser', $scope.user);
			
			//Si el usuario no existe se crea
			if (!$scope.checkUser) {
				socket.emit ('saveUser', registro);
				console.log("Se guarda el nuevo usuario");
			}
		}
	} ] ) ;

	app.controller ('groupController', ['$scope', 'socket', function ($scope, socket) {
		$scope.nombre 		= "";
		$scope.user 		= "";

		if (typeof (Storage) !== 'undefined' ) {
			if (sessionStorage.user != null && sessionStorage.user != "") {
				$scope.user = sessionStorage.user;
			} else {
				window.location.replace("/");
			}
		}

		$scope.registrar = function () {
			if($scope.nombre != ""){

				var registro = {
					nombre: $scope.nombre,
					creador: $scope.user,
					integrantes: [$scope.user]
				}

				socket.emit ('saveGroup', registro);

				window.location.replace("/board");

			}
		}
	} ] ) ;

	app.controller ('asignarController', ['$scope', 'socket', function ($scope, socket) {
		$scope.user 		= "";
		$scope.grupo 		= "";
		$scope.usuario 		= "";

		$scope.listaUsuarios = [];
		$scope.listaGrupos = [];

		if (typeof (Storage) !== 'undefined' ) {
			if (sessionStorage.user != null && sessionStorage.user != "") {
				$scope.user = sessionStorage.user;
				socket.emit ('myGroups', sessionStorage.user);
				socket.emit ('searchUsers', sessionStorage.user);
			} else {
				window.location.replace("/");
			}
		}

		socket.on ('myGroupsResult', function (response, user) {
			console.log( "Escuchó el searchGroupsResult");

			if (user == $scope.user) {
				$scope.listaGrupos = response;

			    console.log ($scope.listaGrupos);

			    $scope.$digest ();
			}
		} );

		socket.on ('searchUsersResult', function (response, user) {
			console.log( "Escuchó el SearchUsersResult");

			if (user == $scope.user) {
				$scope.listaUsuarios = response;

			    console.log ($scope.listaUsuarios);

			    $scope.$digest ();
			}
		} );

		$scope.registrar = function () {
			if($scope.grupo != "" && $scope.usuario != ""){

				var registro = {
					grupo: $scope.grupo,
					user: $scope.usuario
				}

				console.log (registro);

				socket.emit ('asignarUsuario', registro);

				window.location.replace("/board");
			}
		}
	} ] ) ;


	app.controller ('chatController', ['$scope', 'socket', function ($scope, socket) {
		console.log ('Entró a chatController');

		$scope.user = "";
		$scope.username = "";
		$scope.grupo = "";
		//Comprobar la sessión
		if (typeof (Storage) !== 'undefined' ) {
			if (sessionStorage.user != null && sessionStorage.user != "") {
				console.log (sessionStorage.user);
				$scope.user = sessionStorage.user;
				$scope.username = sessionStorage.username;
				socket.emit ('searchGroups', sessionStorage.user);
			} else {
				window.location.replace("/");
			}
		}

		$scope.mensajes = [];
		$scope.grupos = [];

		socket.on ('searchGroupsResult', function (response, user) {
			console.log( "Escuchó el searchGroupsResult");
			console.log (user);

			if (user == $scope.user) {
				$scope.grupos = response;

			    console.log ($scope.grupos);

			    $scope.$digest ();
			}
		} );

		socket.on ('find', function (response, grupo) {
			if(grupo == $scope.grupo) {
				console.log ('escucho evento find');
	    
			    $scope.mensajes = response;
			    
			    console.log ($scope.mensajes);
			    
			    $scope.$digest ();
			}
		});

		$scope.enviarMensaje = function () {
			console.log("entró a enviar mensaje en elgrupo: "+$scope.grupo);
			if ($scope.grupo != "") {
				console.log ('Enviar y guardar Mensaje');
			    
			    var mensaje = {
			      mensaje: $scope.message,
			      usuario: $scope.user,
			      nombreUsuario: $scope.username,
			      grupo: $scope.grupo,
			      aUsuario: ""
			    };
				    
			    socket.emit ('enviarMsg', mensaje);
			    
			    $scope.message = '';
			}

	  	}
		$scope.seleccionarGrupo = function (grupo) {
			console.log(grupo);
			$scope.grupo = grupo;
			socket.emit ('msgsGrupo', grupo);

			if (typeof (Storage) !== 'undefined' ) {
				if (sessionStorage.grupo != null && sessionStorage.grupo != "") {
					sessionStorage.grupo = $scope.grupo;
				} else {
					sessionStorage.grupo = $scope.grupo;
				}
			}
		}
	} ] );

} ) ();
