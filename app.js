var port 				= process.env.PORT || 8000,
	express 			= require ('express'),
	app 				= express (),
	io					= require ('socket.io').
								listen (
									app.listen (port, function () {
										console.log ('listening on *:8000');
									} )
								);
								
var chat = require ('./models/ChatModel') .chatsch; 
var group = require ('./models/GroupModel') .groupssch; 
// var task = require ('./models/TaskModel') .tasksch; 
var user = require ('./models/UserModel') .userssch; 

require ('./config') (app);
require ('./routes') (app);
require ('./events') (io, chat, user, group);
