
//Incluimos express
var express = require ('express');

module.exports = function (app) {

	// Configuramos el engine de las vistas como html
	app.set ('view engine', 'html');

	// iniciamos el engine de las vistas con soporte ejs
	app.engine ('html', require ('ejs') .renderFile);

	// Configuramos la ruta de las vistas
	app.set ('views', __dirname + '/views');

	// Configuramos la carpeta pública
	app.use (express.static (__dirname + '/public') );

};
