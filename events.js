module.exports = function ( io, chat, user, group ) {
    io.sockets.on ( 'connection', function ( socket ) {
        console.log ('Usuario Conectado') ;

        /*
        *Sección de CHAT
        */  
        // chat.find ( {}, function (err, docs) {
        // 	io.emit ('find', docs);
        //     console.log("enviados mensajes chat: " + docs);
        // } );

        socket.on ('msgsGrupo', function ( response ) {
            chat.find ( {grupo: response}, function (err, docs) {
                io.emit ('find', docs, response);
            } );
        } );

        socket.on ('enviarMsg', function ( response ) {
        	var bson_chat = new chat (response);
        	bson_chat.save (function () {
        		console.log ('Guardamos el mensaje');
        		chat.find ( {grupo: response.grupo}, function (err, docs) {
        			io.emit ('find', docs, response.grupo);
        		} );
        	} );
        } );

        /*
        *Sección Registro
        */
        socket.on ('searchUser', function (response) {
            console.log ("entró a searchUser");
            console.log (response);
            user.findOne ( {user: '"'+response+'"' }, function (err, docs) {
                console.log ("ejecutó searchUser");
                io.emit ('searchUserResult', docs);
            } );
        } );

        socket.on ('saveUser', function (response) {
            console.log ("entró a saveUser");
            var bson_user = new user (response);
            bson_user.save ();
        } );

        /*
        *Sección Login
        */
        socket.on ('loginUser', function (response) {
            console.log ("Entró en loginUser");
            user.findOne ( {user: response.user, pass: response.pass}, function (err, docs) {
                console.log ("ejecutó loginUser");
                io.emit ('loginUserResult', docs);
            } );
        } );

        /*
        * Sección grupos
        */
        socket.on ('searchGroups', function (response) {
            group.find ( {"integrantes": response}, function (err, docs) {
                io.emit ('searchGroupsResult', docs, response);
                console.log ("Entró'en buscarGrupos.");
            } );
        } );

        /*
        *Creación de grupos
        */
        socket.on ('saveGroup', function (response) {
            var bson_group = new group (response);
            bson_group.save (function () {
            } );
        } );

        /*
        *Asignación de usuarios a grupos
        */
        socket.on ('myGroups', function (response) {
            group.find ( {"creador": response}, function (err, docs) {
                io.emit ('myGroupsResult', docs, response);
                console.log ("Entró'en buscar mis Grupos.");
            } );
        } );

        socket.on ('searchUsers', function (response) {
            user.find ( {}, function (err, docs) {
                io.emit ('searchUsersResult', docs, response);
                console.log ("Entró'en buscar usuarios.");
            } );
        } );

        socket.on ('asignarUsuario', function (response) {
            //group.update ( {_id: response.grupo}, { $push: { integrantes: response.user } },done);
            group.findByIdAndUpdate(
                response.grupo,
                {$push: {"integrantes": response.user}},
                {safe: true, upsert: true},
                function(err, model) {
                    console.log(err);
                }
            );
        } );        

        socket.on ('disconnect', function () {
            console.log ('Usuario desconectado');
        } );
    } );
};
