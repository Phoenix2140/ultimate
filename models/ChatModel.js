var mongoose = require ('mongoose');
var Schema   = mongoose.Schema;

var db = mongoose.createConnection ('mongodb://localhost/chat');

var chat_schema   = new Schema (
  {
      mensaje: String,
      usuario: String,
      nombreUsuario: String,
      grupo: String,
      aUsuario: String
  }
);

//creamos schema para exportar y llamarlo en otros componentes
var chatsch = db.model ('chatsch', chat_schema);
module.exports.chatsch = chatsch;

//https://www.npmjs.com/package/mongoose

      // usuario: String,
      // nombreUsuario: String,
      // grupo: String,
      // aUsuario: String