var mongoose = require ('mongoose');
var Schema   = mongoose.Schema;

var db = mongoose.createConnection ('mongodb://localhost/users');

var users_schema   = new Schema (
  {
      user: String,
      pass: String,
      email: String,
      pos_x: String,
      pos_y: String
  }
);

//creamos schema para exportar y llamarlo en otros componentes
var userssch = db.model ('userssch', users_schema);
module.exports.userssch = userssch;

//https://www.npmjs.com/package/mongoose