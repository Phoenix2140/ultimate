var mongoose = require ('mongoose');
var Schema   = mongoose.Schema;

var db = mongoose.createConnection ('mongodb://localhost/groups');

var group_schema   = new Schema (
  {
      nombre: String,
      creador: String,
      integrantes: [String]
  }
);

//creamos schema para exportar y llamarlo en otros componentes
var groupssch = db.model ('groupssch', group_schema);
module.exports.groupssch = groupssch;

//https://www.npmjs.com/package/mongoose