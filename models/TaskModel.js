var mongoose = require ('mongoose');
var Schema   = mongoose.Schema;

mongoose.createConnection ('mongodb://localhost/task');

var task_schema   = new Schema (
  {
      user: String,
      fecha_creado: { type: Date, default: Date.now },
      fecha_termino: Date,
      titulo: String,
      mensaje: String,
      grupo: String
  }
);

//creamos schema para exportar y llamarlo en otros componentes
var tasksch = mongoose.model ('tasksch', task_schema);
module.exports.tasksch = tasksch;

//https://www.npmjs.com/package/mongoose